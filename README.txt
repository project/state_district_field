
State District Field module use to select the state and corresponding districts in INDIA.

Installation
------------

Copy State District Field module to your module directory and then enable on the admin modules page.

Uses
------------

Go to user creation page to create new user, select state and district.
Update user via user edit page, choose another state and district.

Note: If you change the state district list according to your country then change the content in CSV file i.e. state_district_field/csv/*.   

